package com.pe.amalosmas;

import com.pe.amalosmas.dao.RESTfulImpl;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ImplementationUnitTest {
    @Test
    public void ObjetivosImpl_isCorrect() {

        RESTfulImpl lectura = new RESTfulImpl();
        System.out.println(Arrays.toString(lectura.getObjetivos()));
        assertEquals("Amabilidad", lectura.getObjetivos()[0]);
    }
    @Test
    public void MisionImpl_isCorrect() {
        RESTfulImpl lectura = new RESTfulImpl();
        System.out.println(lectura.getMision());
        String mision = "Mision";
        assertEquals(mision, lectura.getMision());
    }
    @Test
    public void VisionImpl_isCorrect() {
        RESTfulImpl lectura = new RESTfulImpl();
        System.out.println(lectura.getVision());
        String mision = "Visión";
        assertEquals(mision, lectura.getVision());
    }
    @Test
    public void getHTTP() {
        RESTfulImpl lectura = new RESTfulImpl();
        String url = "https://api.mlab.com/api/1/databases/amalos_mas/collections/?q&apiKey=jYu8XaM-9ROMbTXyYSbov6kHjGFXMbzj";
        System.out.println(lectura.GetHTTPData(url));
        assertEquals("[ \"nosotros\" , \"objectlabs-system\" , \"objectlabs-system.admin.collections\" , \"system.indexes\" ]", lectura.GetHTTPData(url));
    }
}