package com.pe.amalosmas.login.data;

import android.support.annotation.Nullable;

public class PublicDataUserView {


    public final static String getUsername() {
        return LoginRepository.getInstance().getUser().getDisplayName();
    }

    public final static String getCorreo() {
        return LoginRepository.getInstance().getUser().getCorreo();
    }
}
