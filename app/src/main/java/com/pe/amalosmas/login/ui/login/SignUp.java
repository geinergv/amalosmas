package com.pe.amalosmas.login.ui.login;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pe.amalosmas.R;

public class SignUp extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private boolean unlock_password = false;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a1_0_sign_up);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory(Login.SIGN_UP))
                .get(LoginViewModel.class);


        final EditText usernameEditText = findViewById(R.id.a1__txt_email);
        final EditText passwordEditText = findViewById(R.id.a1__txt_password);
        final EditText passwordConfirm = findViewById(R.id.a1_0_txt_password_confirm);

        final Button loginButton = findViewById(R.id.a1__btn_log_in);
        final ImageView a1_btn_eye_password = (ImageView) findViewById(R.id.a1__btn_eye_password);
        final ImageView a1_0_btn_eye_password_confirmed = (ImageView) findViewById(R.id.a1_0_btn_eye_password_confirm);
        final ProgressBar loadingProgressBar = findViewById(R.id.a1__img_loading);
        Button go_to_btn = findViewById(R.id.a1__btn_go_to_sign_other);
        context = this;

        a1_btn_eye_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unlock_password) {
                    unlock_password = false;
                    passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    unlock_password = true;
                    passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
        a1_0_btn_eye_password_confirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unlock_password) {
                    unlock_password = false;
                    passwordConfirm.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    unlock_password = true;
                    passwordConfirm.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
        go_to_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sign_in_activity = new Intent(getApplicationContext(), SignIn.class);
                startActivity(sign_in_activity);
                finish();
            }
        });

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (passwordConfirm.getText().toString().equals(passwordEditText.getText().toString())) {
                        loginViewModel.signup(context, usernameEditText.getText().toString(),
                                passwordEditText.getText().toString(), passwordConfirm.getText().toString());
                    }
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordConfirm.getText().toString().equals(passwordEditText.getText().toString())) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    loginViewModel.signup(context, usernameEditText.getText().toString(),
                            passwordEditText.getText().toString(), passwordConfirm.getText().toString());
                }
            }
        });
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.a1_1_txt_sigup_success) + " para " + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}
