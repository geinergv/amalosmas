package com.pe.amalosmas.login.ui.login;

import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Patterns;

import com.pe.amalosmas.login.data.LoginRepository;
import com.pe.amalosmas.login.data.Result;
import com.pe.amalosmas.login.data.model.LoggedInUser;
import com.pe.amalosmas.R;

public class LoginViewModel extends ViewModel {
    private final int TIPO;

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    LoginViewModel(LoginRepository loginRepository, int tipo) {
        this.loginRepository = loginRepository;
        TIPO = tipo;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(Context activity, String username, String password) {
        // can be launched in a separate asynchronous job
        new GetData(activity, TIPO).execute(username, password);
    }

    public void signup(Context activity, String username, String password, String confirmPassword) {
        new GetData(activity, TIPO).execute(username, password);
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }

    private void setLoginResult(LoggedInUser data) {
        loginResult.setValue(new LoginResult(new LoggedInUserView(data.getDisplayName(), data.getCorreo())));
    }

    public void updateLoginResult() {
        setLoginResult(loginRepository.getUser());
    }

    class GetData extends AsyncTask<String, Void, Result<LoggedInUser> > {
        ProgressDialog pd;
        Context context;
        final int TIPO;

        public GetData(Context context, int tipo) {
            this.pd = new ProgressDialog(context);
            TIPO = tipo;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setTitle("Validando...");
            pd.show();
        }
        @Override
        protected Result<LoggedInUser> doInBackground(String... params) {
            Result<LoggedInUser> result = null;
            String para0 = params[0];
            String para1 = params[1];
            if (TIPO == Login.SIGN_UP) {
                System.out.println("Registro: " + para0 + " " + para1);
                result = loginRepository.signup(para0, para1);
            } else if (TIPO == Login.SIGN_IN) {
                System.out.println("Login: " + para0 + " " + para1);
                result = loginRepository.login(para0, para1);
            }
            return result;
        }

        @Override
        protected void onPostExecute(Result<LoggedInUser> result) {
            super.onPostExecute(result);
            pd.dismiss();
            if (result instanceof Result.Success) {
                LoggedInUser data = ((Result.Success<LoggedInUser>) result).getData();
                setLoginResult(data);
            } else {
                int recurso = TIPO == Login.SIGN_IN ? R.string.sign_in_failed : R.string.sign_in_failed;
                loginResult.setValue(new LoginResult(recurso));
            }
        }
    }

    public void logout() {
        loginRepository.logout();
    }

    public void loadUserFromLocal(SharedPreferences preferences) {
        loginRepository.loadUserFromLocal(preferences);
    }
}
