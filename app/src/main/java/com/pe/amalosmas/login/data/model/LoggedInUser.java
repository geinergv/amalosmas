package com.pe.amalosmas.login.data.model;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {
    public static final String PREFS_NAME = "USER_LOGGED";
    private static final String PREF_ID = "_id";
    private static final String PREF_CORREO = "correo";
    private static final String PREF_USERNAME = "nombre";
    private static final String PREF_PASSWORD = "pwd";

    private String userId;
    private String displayName;
    private String correo;
    private String password;

    public LoggedInUser(String userId, String displayName, String correo, String password) {
        this.userId = userId;
        this.displayName = displayName;
        this.correo = correo;
        this.password = password;
    }

    public String getCorreo() {
        return correo;
    }

    public String getPassword() {
        return password;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPrefId() {
        return PREF_ID;
    }

    public String getPrefCorreo() {
        return PREF_CORREO;
    }

    public String getPrefUsername() {
        return PREF_USERNAME;
    }

    public String getPrefPassword() {
        return PREF_PASSWORD;
    }
}
