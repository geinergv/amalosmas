package com.pe.amalosmas.login.ui.login;

/**
 * Class exposing authenticated user details to the UI.
 */
class LoggedInUserView {
    private String displayName;
    private String correo;
    //... other data fields that may be accessible to the UI

    public LoggedInUserView(String displayName, String correo) {
        this.displayName = displayName;
        this.correo = correo;
    }

    String getDisplayName() {
        return displayName;
    }

    String getCorreo() {
        return correo;
    }
}
