package com.pe.amalosmas.login.data;

import com.pe.amalosmas.dao.RESTfulImpl;
import com.pe.amalosmas.login.data.model.LoggedInUser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    private RESTfulImpl dbAccess = new RESTfulImpl();

    private static final String COLLECCION = "usuarios";

    public Result<LoggedInUser> login(String correo, String password) {
        String stream= null;
        try {
            String urlStr = RESTfulImpl.setURIBase(COLLECCION);
            urlStr += String.format("&q=%s&f=%s&fo=true", new JSONObject().put("correo", correo).put("pwd", password),
                    new JSONObject().put("nombre", 1));
            stream = dbAccess.GetHTTPData(urlStr);
            //JSONObject json = .getJSONObject(0);
            if (!stream.trim().equals(null)) {
                JSONObject json = new JSONObject(stream);
                String nombre = json.has("nombre") ? json.getString("nombre") : "";
                LoggedInUser user = new LoggedInUser(
                        json.getJSONObject("_id").getString("$oid"), nombre, correo, password
                );
                return new Result.Success<>(user);
            } else {
                throw new Exception("Credenciales inválidas");
            }
        } catch (Exception e) {
            return new Result.Error(new IOException(e.getMessage().isEmpty() ? "Error al iniciar sesión" : e.getMessage(), e));
        }

    }

    public Result<LoggedInUser> signup(String correo, String password) {
        String stream= null;
        try {
            String urlStr = String.format("%s&c=true&q=%s",
                    RESTfulImpl.setURIBase(COLLECCION),
                    new JSONObject().put("correo", correo)
                    );
            stream = dbAccess.GetHTTPData(urlStr);

            int cantidad=-1;
            try {
                cantidad = Integer.parseInt(stream);
            } catch (Exception e) {}

            System.out.println(cantidad + "  " + (cantidad ==0));

            if (cantidad==0) {
                JSONArray datos = dbAccess.PostHTTPData(
                        RESTfulImpl.setURIBase(COLLECCION),
                        new JSONObject().put("correo", correo).put("pwd", password).toString()
                );
                if (datos.length()>0) {
                    LoggedInUser user = new LoggedInUser(
                            datos.getJSONObject(0).getJSONObject("_id").getString("$oid"),
                            "",
                            correo, password
                    );
                    return new Result.Success<>(user);
                } else {
                    throw new Exception("Error al intentar registrarse");
                }
            } else {
                throw new Exception("Correo existente. Ya sea creado una cuenta con este correo.");
            }
        } catch (Exception e) {
            return new Result.Error(new IOException(e.getMessage().isEmpty() ? "Error al iniciar sesión" : e.getMessage(), e));
        }

    }

    public void logout() {
        // TODO: revoke authentication
    }
}
