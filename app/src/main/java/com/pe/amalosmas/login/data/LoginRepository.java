package com.pe.amalosmas.login.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.pe.amalosmas.ContinuarCon;
import com.pe.amalosmas.login.data.model.LoggedInUser;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;

    private LoginDataSource dataSource;

    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore
    private LoggedInUser user = null;

    // private constructor : singleton access
    private LoginRepository(LoginDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static LoginRepository getInstance(LoginDataSource dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public static LoginRepository getInstance() {
        if (instance == null) {
            instance = new LoginRepository(new LoginDataSource());
        }
        return instance;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void logout() {
        user = null;
        dataSource.logout();
    }

    private void setLoggedInUser(LoggedInUser user) {
        this.user = user;
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        //SharedPreferences preferences = getSharedPreferences(user.PREFS_NAME, 0);
                //.edit()
                //.putString(PREF_USERNAME, username)
                //.putString(PREF_PASSWORD, password)
                //.commit();

    }

    public void saveUserInLocal(SharedPreferences preferences) {
        preferences.edit()
                .putString(user.getPrefId(), user.getUserId())
                .putString(user.getPrefUsername(), user.getDisplayName())
                .putString(user.getPrefCorreo(), user.getCorreo())
                .putString(user.getPrefPassword(), user.getPassword())
                .apply();
    }

    public String[][] getDataToSave() {
        return new String[][]{
                {user.getPrefId(), user.getUserId()},
                {user.getPrefUsername(), user.getDisplayName()},
                {user.getPrefCorreo(), user.getCorreo()},
                {user.getPrefPassword(), user.getPassword()}
        };
    }

    public void loadUserFromLocal(SharedPreferences pref) {
        String idUser = pref.getString(user.getPrefId(), "");
        String password = pref.getString(user.getPrefPassword(), "");
        System.out.println(" ===================== BEFORE ===================");
        if (idUser != null && password != null) {
            String username = pref.getString(user.getPrefUsername(), null);
            username = username==null ? "" : username;
            String correo = pref.getString(user.getPrefCorreo(), null);
            correo = correo==null ? "" : correo;
            setLoggedInUser(new LoggedInUser(
                    idUser, username, correo, password
            ));
        }
    }

    public void loadUserFromLocal(String... params) {
        user = new LoggedInUser(
            params[0], params[1], params[2], params[3]
        );
    }

    public LoggedInUser getUser() {
        return user;
    }

    public Result<LoggedInUser> login(String username, String password) {
        // handle login
        Result<LoggedInUser> result = dataSource.login(username, password);
        if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
        }
        return result;
    }

    public Result<LoggedInUser> signup(String username, String password) {
        // handle login
        Result<LoggedInUser> result = dataSource.signup(username, password);
        if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
        }
        return result;
    }
}
