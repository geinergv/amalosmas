package com.pe.amalosmas;

import android.accounts.AccountManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.pe.amalosmas.login.data.LoginDataSource;
import com.pe.amalosmas.login.data.LoginRepository;
import com.pe.amalosmas.login.data.model.LoggedInUser;
import com.pe.amalosmas.login.ui.login.Login;
import com.pe.amalosmas.login.ui.login.LoginViewModel;
import com.pe.amalosmas.login.ui.login.LoginViewModelFactory;

public class Init extends AppCompatActivity {
	private ImageView wait;
	private static final String PREF_ID = "_id";
	private static final String PREF_CORREO = "correo";
	private static final String PREF_USERNAME = "nombre";
	private static final String PREF_PASSWORD = "pwd";
	private LoginRepository loginRepository = LoginRepository.getInstance(new LoginDataSource());;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a0_init);
		getData();
		Intent i = null;
		if (loginRepository.isLoggedIn()) {
			i = new Intent(getApplicationContext(), ActividadPrincipal.class);
		} else {
			i = new Intent(getApplicationContext(), ContinuarCon.class);
		}
		startActivity(i);
		finish();
	}

	public void getData() {
		SharedPreferences preferences =  getSharedPreferences(LoggedInUser.PREFS_NAME, MODE_PRIVATE);
		String id = preferences.getString(PREF_ID, null);
		String pass = preferences.getString(PREF_PASSWORD, null);
		if (id != null && pass != null) {
			String nombre = preferences.getString(PREF_USERNAME, null);
			String correo = preferences.getString(PREF_CORREO, null);
			loginRepository.loadUserFromLocal(id, nombre, correo, pass);
		}
	}

	/*
	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == CAN_CONTINUE) {
			if (resultCode == RESULT_OK) {
				String correo = new String(data.getCharArrayExtra("correo"));
				String password = new String(data.getCharArrayExtra("pass"));
				SharedPreferences preferences = getPreferences()
			}
		}
	}*/


}
