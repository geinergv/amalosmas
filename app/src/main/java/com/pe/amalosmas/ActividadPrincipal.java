package com.pe.amalosmas;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.pe.amalosmas.login.data.LoginDataSource;
import com.pe.amalosmas.login.data.LoginRepository;
import com.pe.amalosmas.login.data.PublicDataUserView;
import com.pe.amalosmas.login.data.model.LoggedInUser;
import com.pe.amalosmas.login.ui.login.Login;
import com.pe.amalosmas.login.ui.login.LoginViewModel;
import com.pe.amalosmas.login.ui.login.LoginViewModelFactory;
import com.pe.amalosmas.main.Experiencias;
import com.pe.amalosmas.main.Nosotros;
import com.pe.amalosmas.main.Servicios;

public class ActividadPrincipal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static Integer id_frame = null;
    private LoginRepository loginRepository = LoginRepository.getInstance(new LoginDataSource());;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        System.out.println((PublicDataUserView.getUsername().isEmpty())+ "  "+PublicDataUserView.getCorreo());
        //((TextView) findViewById(R.id.am_username)).setText(PublicDataUserView.getUsername());
        //((TextView) findViewById(R.id.am_user_correo)).setText(PublicDataUserView.getCorreo());
        selectItemNav(R.id.nav_experience);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (id_frame == R.id.nav_home || id_frame == R.id.nav_services) {
            selectItemNav(R.id.nav_experience);
        } else  {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        selectItemNav(id);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout() {
        getSharedPreferences(LoggedInUser.PREFS_NAME, MODE_PRIVATE).edit()
                .clear().apply();

        loginRepository.logout();

        Intent i = new Intent(getApplicationContext(), ContinuarCon.class);
        startActivity(i);
        finish();
    }

    public void selectItemNav(int id) {
        Fragment fragment = null;
        if (id == R.id.nav_home) {
            fragment = new Nosotros();
        } else if (id == R.id.nav_services) {
            fragment = Servicios.newInstance(findViewById(R.id.content_frame).getWidth(), findViewById(R.id.content_frame).getHeight());
        } else if (id == R.id.nav_experience) {
            fragment = new Experiencias();
        } else if (id == R.id.nav_logout) {
            logout();
        }

        if (fragment != null) {
            id_frame = id;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }
}
