package com.pe.amalosmas.dao;
//
//import com.mashape.unirest.http.HttpResponse;
//import com.mashape.unirest.http.JsonNode;
//import com.mashape.unirest.http.Unirest;
//import com.mashape.unirest.http.exceptions.UnirestException;

//import org.apache.http.message.BasicHttpResponse;


import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class RESTfulImpl {
    private static final String APIKEY = "jYu8XaM-9ROMbTXyYSbov6kHjGFXMbzj";
    private static final String DB = "amalos_mas";
    private static final String BASEURI = "https://api.mlab.com/api/1/databases/%s/collections/%s";

    public RESTfulImpl() {}

    static String stream = null;

    public static String setURIBase(String collection, @Nullable String db) {
        String urlString = String.format(BASEURI, (db != null ? db : DB), collection);
        urlString += "?apiKey="+APIKEY;
        return urlString;
    }

    public static String setURIBase(String collection) {
        return setURIBase(collection, null);
    }

    public String GetHTTPData(String urlString) {
        stream = null;
        try {
            URL url = new URL(urlString);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == 200) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null)
                    sb.append(line);
                stream = sb.toString();
                urlConnection.disconnect();

            } else {

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    public JSONArray PostHTTPData(String urlString, String dataJson) {
        stream = null;
        System.out.println(dataJson);
        try {
            URL url = new URL(urlString);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            byte[] out = dataJson.getBytes();
            int len = out.length;

            urlConnection.setFixedLengthStreamingMode(len);
            urlConnection.setRequestProperty("Content-Type","application/json; charset=UTF-8" );
            urlConnection.connect();

            try {
                OutputStream os = urlConnection.getOutputStream();
                os.write(out);
            } catch (Exception e) {}

            InputStream response = urlConnection.getInputStream();

            BufferedReader r = new BufferedReader(new InputStreamReader(response));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null)
                sb.append(line);
            stream = sb.toString();
            urlConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray result = null;
        try {
            result = new JSONArray(stream);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /*public String getQueries(JSONObject data) {

    }*/

    /*public JSONArray getDataNosotros(int tipo) {
        String uri = String.format(BASEURI, DB, "nosotros");
        HttpResponse<JsonNode> resp = null;
        System.out.println("view");
        try {
            resp = Unirest.get(uri)
                    .queryString("q", "{\"tipo\":"+tipo+"}")
                    .queryString("apiKey", APIKEY).asJson();
        } catch (UnirestException ex) {
            System.out.println("Error al realizar petición");
        } catch (Exception e) {
            System.out.println("Error general");
        }
        return resp.getBody().getArray();
    }*/

    public String getMision() {
        String result = null;
        /*JSONArray messages = getDataNosotros(0);
        JSONObject data = null;
        try {
            data = messages.getJSONObject(0);
            result = data.getString("valor");
        } catch (JSONException ex) {

        }*/
        result = GetHTTPData("https://api.mlab.com/api/1/databases/amalos_mas/collections/nosotros?q={%22tipo%22:0}&apiKey=jYu8XaM-9ROMbTXyYSbov6kHjGFXMbzj");
        return result;
    }

    public String getVision() {
        String result = null;
        /*JSONArray messages = getDataNosotros(1);
        JSONObject data = null;
        try {
            data = messages.getJSONObject(0);
            result = data.getString("valor");
        } catch (JSONException ex) {

        }*/
        result = GetHTTPData("https://api.mlab.com/api/1/databases/amalos_mas/collections/nosotros?q={%22tipo%22:1}&apiKey=jYu8XaM-9ROMbTXyYSbov6kHjGFXMbzj");
        return result;
    }

    public String[] getObjetivos() {
        String[] result = null;
        /*JSONArray messages = getDataNosotros(2);
        ArrayList<String> resultados = new ArrayList<String>();
        JSONObject data = null;
        for (int i = 0; i < messages.length(); i++) {
            try {
                data = messages.getJSONObject(i);
            } catch (JSONException e) {
                data = new JSONObject();
            }
            try {
                resultados.add(data.getString("valor"));
            } catch (JSONException e) {

            }
        }
        result = resultados.toArray(new String[resultados.size()]);*/
        return result;
    }

    /*public void close() {
        cliente.close();
    }*/
}
