package com.pe.amalosmas;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pe.amalosmas.login.data.LoginDataSource;
import com.pe.amalosmas.login.data.LoginRepository;
import com.pe.amalosmas.login.data.model.LoggedInUser;
import com.pe.amalosmas.login.ui.login.Login;
import com.pe.amalosmas.login.ui.login.LoginViewModel;
import com.pe.amalosmas.login.ui.login.LoginViewModelFactory;
import com.pe.amalosmas.login.ui.login.SignIn;
import com.pe.amalosmas.login.ui.login.SignUp;

public class ContinuarCon extends AppCompatActivity {
    public static final int REQUEST_SIGN_IN = 1;
    public static final int REQUEST_SIGN_UP = 0;
    private int activity_iniciada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a1_continuar_con);

        Button a1_btn_sign_in = (Button) findViewById(R.id.a1_btn_go_to_sign_in);
        TextView a1_btn_go_to_sign_up = (TextView) findViewById(R.id.a1_txt_go_to_sign_up);

        a1_btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sign_in_activity = new Intent(getApplicationContext(), SignIn.class);
                activity_iniciada = REQUEST_SIGN_IN;
                startActivityForResult(sign_in_activity, REQUEST_SIGN_IN);
            }
        });
        a1_btn_go_to_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sign_up_activity = new Intent(getApplicationContext(), SignUp.class);
                activity_iniciada = REQUEST_SIGN_IN;
                startActivityForResult(sign_up_activity, REQUEST_SIGN_UP);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SIGN_IN  || requestCode == REQUEST_SIGN_UP) {
            if (resultCode == RESULT_OK) {
                LoginRepository loginRepository = LoginRepository.getInstance(new LoginDataSource());
                if (loginRepository.isLoggedIn()) {
                    String p[][] = loginRepository.getDataToSave();
                    saveDataInLocal(p[0], p[1],p[2],p[3]);
                }
                Intent i = new Intent(getApplicationContext(), ActividadPrincipal.class);
                startActivity(i);
                finish();
            }
        }
    }

    private void saveDataInLocal(String[]... params) {
        getSharedPreferences(LoggedInUser.PREFS_NAME, MODE_PRIVATE).edit()
                .putString(params[0][0], params[0][1])
                .putString(params[1][0], params[1][1])
                .putString(params[2][0], params[2][1])
                .putString(params[3][0], params[3][1])
                .apply();
    }

}
