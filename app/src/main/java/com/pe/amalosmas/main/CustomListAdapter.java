package com.pe.amalosmas.main;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pe.amalosmas.R;

public class CustomListAdapter extends ArrayAdapter {
    //to reference the Activity
    private final Activity context;

    //to store the list of countries
    private final String[] nameArray;

    //to store the list of countries
    private final int[] infoArray;


    public CustomListAdapter(Activity context, String[] nameArrayParam, int[] infoArrayParam){
        super(context, R.layout.yoga_row , nameArrayParam);
        this.context=context;
        this.nameArray = nameArrayParam;
        this.infoArray = infoArrayParam;
    }
    public CustomListAdapter(Activity context, String[] nameArrayParam){
        super(context, R.layout.yoga_row , nameArrayParam);
        this.context=context;
        this.nameArray = nameArrayParam;
        System.out.println(nameArrayParam);
        this.infoArray = new int[0];
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.yoga_row, null,true);

        //this code gets references to objects in the listview_row.xml file
        TextView nameTextField = rowView.findViewById(R.id.nombre_empresa);
        //TextView infoTextField = (TextView) rowView.findViewById(R.id.infoTextViewID);
        //ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView1ID);

        //this code sets the values of the objects to values from the arrays
        nameTextField.setText(nameArray[position]);
        //infoTextField.setText(infoArray[position]);
        //imageView.setImageResource(imageIDarray[position]);

        return rowView;

    };
}
