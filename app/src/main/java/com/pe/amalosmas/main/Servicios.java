package com.pe.amalosmas.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pe.amalosmas.ActividadPrincipal;
import com.pe.amalosmas.R;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Servicios.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Servicios#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Servicios extends Fragment {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "cnt_wid";
	private static final String ARG_PARAM2 = "cnt_hei";

	final int ARMA_PAQUETE = 0;

	// TODO: Rename and change types of parameters
	private int cntWid;
	private int cntHei;
	private int itmSiz;
	private double radio;

	private OnFragmentInteractionListener mListener;

	public Servicios() {
		// Required empty public constructor
	}

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param cntWid Parameter 1.
	 * @param cntHei Parameter 2.
	 * @return A new instance of fragment Servicios.
	 */
	// TODO: Rename and change types and number of parameters
	public static Servicios newInstance(int cntWid, int cntHei) {
		Servicios fragment = new Servicios();
		Bundle args = new Bundle();
		args.putInt(ARG_PARAM1, cntWid);
		args.putInt(ARG_PARAM2, cntHei);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			cntWid = getArguments().getInt(ARG_PARAM1);
			cntHei = getArguments().getInt(ARG_PARAM2);
			int minVal = cntHei>cntWid ? cntWid : cntHei;
			itmSiz = Math.round(minVal/5);
			radio = itmSiz*1.7;
		}
	}

	public int[][] getPositionsServices() {
		double longitudAngular = 2*radio*Math.PI;
		int itmsSeparacion = 1;
		int itmsTotales = 6+itmsSeparacion;
		double anguloTotal = 2*Math.PI;
		double anguloeparacion = anguloTotal/itmsTotales;
		double giroInicial = 0;

		int[][] posiciones = new int[itmsTotales-itmsSeparacion][2];
		int x, y;
		for (int i = 0; i < posiciones.length ; i++) {
			x = (int) (cntWid/2 + radio*Math.cos(giroInicial) - itmSiz*0.75);
			y = (int) (cntHei/2 + radio*Math.sin(giroInicial) - itmSiz*0.75);
			giroInicial += anguloeparacion + anguloeparacion/itmsTotales;
			posiciones[i][0] = x;
			posiciones[i][1] = y;
		}
		return posiciones;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment

		return inflater.inflate(R.layout.fragment_servicios, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		//you can set the title for your toolbar here for different fragments different titles
		ImageView[] servicios = new ImageView[] {
				getView().findViewById(R.id.am_f2_img_servicio1),
				getView().findViewById(R.id.am_f2_img_servicio2),
				getView().findViewById(R.id.am_f2_img_servicio3),
				getView().findViewById(R.id.am_f2_img_servicio4),
				getView().findViewById(R.id.am_f2_img_servicio5),
				getView().findViewById(R.id.am_f2_img_servicio6)
		};
		int[][] posiciones = getPositionsServices();
		int i = 0;
		for (ImageView servicio: servicios) {
			servicio.setMaxHeight(itmSiz);
			servicio.setMaxWidth(itmSiz);
			servicio.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					clickImgService(v);
				}
			});
			servicio.setTranslationX(posiciones[i][0]);
			servicio.setTranslationY(posiciones[i][1]);
			i++;
		}
		ImageView centralService = getView().findViewById(R.id.am_f2_img_servicio7);
		int centralSiz = (int) itmSiz*5/4;
		centralService.setMaxWidth(itmSiz);
		centralService.setMaxHeight(itmSiz);
		centralService.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), Paquete.class);
                startActivityForResult(i, ARMA_PAQUETE);
			}
		});
		getActivity().setTitle(R.string.am_f2_label);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ARMA_PAQUETE) {
			if (resultCode == RESULT_OK) {
				Snackbar.make(getActivity().findViewById(R.id.ServiciosRootLayout), "Gracias por su compra. Se le envió un correo con información relevante sobre su pedido.", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		}
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnFragmentInteractionListener) {
			mListener = (OnFragmentInteractionListener) context;
		} else {
			//throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		void onFragmentInteraction(Uri uri);
	}

	public void clickImgService(View view) {
		Intent i = new Intent(getContext(), YogaActivity.class);
		System.out.println(view.getContentDescription());
		i.putExtra("servicio", view.getContentDescription());
		startActivity(i);
	}
}
