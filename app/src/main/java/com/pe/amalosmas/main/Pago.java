package com.pe.amalosmas.main;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pe.amalosmas.ActividadPrincipal;
import com.pe.amalosmas.R;

public class Pago extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago);
        Button btn = findViewById(R.id.button);
        final EditText dir = findViewById(R.id.editText2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dir.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Complete su dirección", Toast.LENGTH_SHORT).show();
                } else {
                    setResult(RESULT_OK);

                    //Complete and destroy login activity once successful
                    finish();
                }
            }
        });
    }
}
