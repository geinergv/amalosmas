package com.pe.amalosmas.main;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.pe.amalosmas.R;
import com.pe.amalosmas.dao.RESTfulImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

public class Empresa extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        final String nombre = bundle.getString("nombre");
        setTitle(nombre);

        new GetData().execute(nombre);
    }

    class GetData extends AsyncTask<String, Void, String[] > {

        @Override
        protected String[] doInBackground(String... strings) {
            String stream = null;
            String empresa = strings[0];
            RESTfulImpl bd = new RESTfulImpl();
            String urlStr = null;
            try {
                urlStr = String.format("%s&q=%s&f=%s&fo=true",
                        RESTfulImpl.setURIBase("empresas"),
                        new JSONObject().put("nombre", empresa),
                        new JSONObject().put("direccion", 1).put("dsc", 1)
                );


                stream = bd.GetHTTPData(urlStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new String[] {stream};
        }

        @Override
        protected void onPostExecute(String[] strings) {
            super.onPostExecute(strings);
            TextView dsc = findViewById(R.id.am_f1_txt_dsc);
            TextView dir = findViewById(R.id.am_f1_txt_dsc_dir);
            ImageView img = findViewById(R.id.a1__img_logo_completo);
            JSONObject data = null;

            try {
                if (false) {
                    data= new JSONObject(strings[0]);
                } else {
                    data= new JSONObject()
                            .put("dsc",
                                    "Brindar un servicio integral y de calidad orientado a mejorar el bienestar y salud de nuestros miembros, a través de una amplia experiencia y con las últimas tendencias del sector, soportados por una organización comprometida y capacitada que, a su vez, permita un crecimiento rentable y sostenible."
                                    )
                            .put("direccion",
                                    "Ca. Parque Gonzales Prada 787, Magdalena"
                                    )
                            .put("_id", 0)
                    ;
                }
                dsc.setText(data.getString("dsc"));
                dir.setText(data.getString("direccion"));
                int recurso = R.mipmap.dsc_general;
                switch (data.getInt("_id")) {
                    case 1:
                        recurso = R.mipmap.e1serv;
                        break;
                    case 2:
                        recurso = R.mipmap.e2serv;
                        break;
                    case 3:
                        recurso = R.mipmap.e3serv;
                        break;
                    case 4:
                        recurso = R.mipmap.e4serv;
                        break;
                    case 5:
                        recurso = R.mipmap.e5serv;
                        break;
                }
                img.setBackgroundResource(recurso);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
