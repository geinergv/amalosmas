package com.pe.amalosmas.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pe.amalosmas.R;
import com.pe.amalosmas.dao.RESTfulImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class YogaActivity extends AppCompatActivity {
    private JSONObject empresa;
    private TextView txtEmp;
    private boolean canNext = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yoga);
        Bundle bundle = getIntent().getExtras();
        final String servicio = bundle.getString("servicio");

        setTitle(servicio);
        new GetData(this).execute(servicio);

        ListView view = findViewById(R.id.listviewID);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), Empresa.class);
                i.putExtra("nombre",
                        ((TextView)((LinearLayout) view).getChildAt(0)).getText().toString()
                );
                startActivity(i);
            }
        });

        //view.setOnItemClickListener();

        /*txtEmp = findViewById(R.id.empresa1);
        setTitle(servicio);
        new GetData().execute(servicio);

        txtEmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (canNext) {
                    Intent i = new Intent(getApplicationContext(), Empresa.class);
                    i.putExtra("servicio", servicio);
                    try {
                        i.putExtra("nombre", empresa.getString("nombre"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(i);
                }
            }
        });*/
    }

    class GetData extends AsyncTask<String, Void, JSONArray > {
        private Activity contecxt;

        public GetData(Activity contecxt) {
            this.contecxt = contecxt;
        }

        @Override
        protected JSONArray doInBackground(String... strings) {
            String stream = null;
            RESTfulImpl bd = new RESTfulImpl();

            String urlStr = null;
            JSONArray array = null;
            try {
                urlStr = String.format("%s&q=%s&f=%s&fo=true",
                        RESTfulImpl.setURIBase("servicios"),
                        new JSONObject().put("nombre", strings[0]),
                        new JSONObject().put("_id", 1)
                );

                System.out.println(urlStr);
                stream = bd.GetHTTPData(urlStr);
                //JSONObject json = new JSONObject(stream);
                System.out.println(stream);
                urlStr = String.format("%s&q=%s&f=%s",
                        RESTfulImpl.setURIBase("empresas"),
                        new JSONObject().put("servicio",new JSONObject(stream).getInt("_id")),
                        new JSONObject().put("nombre", 1)
                );
                System.out.println(urlStr);
                stream = bd.GetHTTPData(urlStr);
                System.out.println(stream);
                array = new JSONArray(stream);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return array;
            /*urlStr += "&q="+json.toString();
            stream = bd.GetHTTPData(urlStr);
            String nombre = null;
            try {
                JSONObject service = new JSONArray(stream).getJSONObject(0);
                urlStr = RESTfulImpl.setURIBase("empresas");
                json.remove("nombre");
                json.put("_id", service.getInt("empresa"));
                urlStr += "&q="+json.toString();

                json.remove("_id");
                json.put("nombre", 1);
                urlStr += "&f="+json.toString();
                stream = bd.GetHTTPData(urlStr);
                service = new JSONArray(stream).getJSONObject(0);
                empresa = service;
                nombre = empresa.getString("nombre");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new String[] {nombre};*/
        }

        @Override
        protected void onPostExecute(JSONArray array) {
            super.onPostExecute(array);
            ListView view = findViewById(R.id.listviewID);
            String[] data = new String[array.length()];
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject empresa = array.getJSONObject(i);
                    data[i] = empresa.getString("nombre");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            view.setAdapter(new CustomListAdapter(contecxt, data));
        }
    }
}
