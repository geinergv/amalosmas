package com.pe.amalosmas.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.pe.amalosmas.R;
import com.pe.amalosmas.dao.RESTfulImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.jar.Attributes;

public class Paquete extends AppCompatActivity {
    private LinearLayout[] groups = new LinearLayout[6];
    //private RadioButton[][] empresas;
    private RESTfulImpl db = null;

    final int GO_TO_PAGO = 0;

    final static String[] DIAS = new String[] {"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Arma tu paquete");
        setContentView(R.layout.activity_paquete);

        groups[0] = findViewById(R.id.lg1);
        groups[1] = findViewById(R.id.lg2);
        groups[2] = findViewById(R.id.lg3);
        groups[3] = findViewById(R.id.lg4);
        groups[4] = findViewById(R.id.lg5);
        groups[5] = findViewById(R.id.lg6);

        for(LinearLayout g: groups) {
            g.setVisibility(View.GONE);
        }
        ((LinearLayout) findViewById(R.id.am_a0_layout_lista_paquete)).setVisibility(View.VISIBLE);
        db = new RESTfulImpl();
        for (int i = 1; i <= groups.length ; i++) {
            new GetData().execute(i);
        }
        Button btn = findViewById(R.id.pagar);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int counter = 0;
                for (LinearLayout lg: groups) {
                    if (((RadioGroup) lg.getChildAt(1)).getCheckedRadioButtonId()!= -1) counter++;
                }
                if (counter==4) {
                    Intent i = new Intent(getApplicationContext(), Pago.class);
                    startActivityForResult(i, GO_TO_PAGO);
                } else {
                    Snackbar.make(findViewById(R.id.srollpago), "Por favor verifique que haya seleccionado unicamente 4 de nuestros servicios", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GO_TO_PAGO) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }
        }
    }

    class GetData extends AsyncTask<Integer, Void, JSONObject > {

        @Override
        protected JSONObject doInBackground(Integer... nums) {
            String stream = null;
            String urlStr = null;
            JSONObject json = null;
            try {
                // Get nombre servicio
                urlStr = String.format("%s&q=%s&fo=true",
                        RESTfulImpl.setURIBase("servicios"),
                        new JSONObject().put("_id", nums[0]),
                        new JSONObject().put("nombre", 1)
                        );
                System.out.println(urlStr);
                stream = db.GetHTTPData(urlStr);
                System.out.println(stream);

                // Get numero empresas
                urlStr = String.format("%s&q=%s&c=true",
                        RESTfulImpl.setURIBase("empresas"),
                        new JSONObject().put("servicio", nums[0])
                );
                json = new JSONObject(stream);
                stream = db.GetHTTPData(urlStr);
                json.put("NUM_EMPRESAS", Integer.parseInt(stream));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);
            if (json != null) {
                int i = 0;
                String nombre = null;
                try {
                    i = json.getInt("_id");
                    nombre = json.getString("nombre");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int layout_pos = i-1;
                System.out.println("nombre ================ " + nombre);
                ((TextView) groups[layout_pos].getChildAt(0)).setText(nombre);
                ((TextView) groups[layout_pos].getChildAt(0)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((RadioGroup)((LinearLayout) v.getParent()).getChildAt(1)).clearCheck();
                    }
                });
                ((RadioGroup) groups[layout_pos].getChildAt(1)).setVisibility(View.GONE);

                try {
                    if (json.has("NUM_EMPRESAS") && json.getInt("NUM_EMPRESAS")>0) {
                        groups[layout_pos].setVisibility(View.VISIBLE);
                        new GetDataEmpresa().execute(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private static final String TXT_TO_DESMARCAR = " (-)";

    class GetDataEmpresa extends AsyncTask<Integer, Void, JSONArray > {
        @Override
        protected JSONArray doInBackground(Integer... nums) {
            String stream = null;
            String urlStr = null;
            JSONArray array = null;
            try {
                urlStr = String.format("%s&q=%s&l=5",
                        RESTfulImpl.setURIBase("empresas"),
                        new JSONObject().put("servicio", nums[0]),
                        new JSONObject().put("nombre", 1).put("dias", 1).put("horario", 1)
                );
                System.out.println(urlStr);
                stream = db.GetHTTPData(urlStr);
                System.out.println(stream);
                array = new JSONArray(stream).put(nums[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return array;
        }

        @Override
        protected void onPostExecute(JSONArray array) {
            super.onPostExecute(array);
            int num_empresas = array.length()-1;
            int layout_pos = 0;
            try {
                layout_pos = array.getInt(num_empresas)-1;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println("num_empresas ========= " + num_empresas);
            for (int i = 0; i < num_empresas; i++) {
                try {
                    JSONObject json = array.getJSONObject(i);
                    RadioButton rbtn = ((RadioButton)((RadioGroup) groups[layout_pos].getChildAt(1)).getChildAt(i));
                    String dsc = json.getString("nombre");
                    if (json.has("dias") && json.getJSONArray("dias").length()>0) {
                        dsc += "  ";
                        JSONArray dias = json.getJSONArray("dias");
                        for (int j = 0; j < dias.length(); j++) {
                            dsc += DIAS[dias.getInt(j)-1].substring(0, 1)+ ", ";
                        }
                        dsc = dsc.substring(0, dsc.length()-2);
                    }
                    if (json.has("horario") &&
                            !json.getJSONObject("horario").getString("from").trim().isEmpty() &&
                            !json.getJSONObject("horario").getString("to").trim().isEmpty()) {
                        dsc += " " + json.getJSONObject("horario").getString("from");
                        dsc += " - " + json.getJSONObject("horario").getString("from");
                    }
                    rbtn.setText(dsc);
                    ((RadioGroup) rbtn.getParent()).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            TextView servicio = ((TextView)((LinearLayout)group.getParent()).getChildAt(0));
                            String txt =servicio.getText().toString();
                            if (checkedId != -1) {
                                if (!txt.endsWith(TXT_TO_DESMARCAR)) {
                                    txt += TXT_TO_DESMARCAR;
                                    servicio.setText(txt);
                                }
                            } else {
                                txt = servicio.getText().toString();
                                if (txt.endsWith(TXT_TO_DESMARCAR)) {
                                    txt = txt.substring(0, txt.length() - TXT_TO_DESMARCAR.length());
                                    servicio.setText(txt);
                                }
                            }
                        }
                    });
                    /*rbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            TextView servicio = ((TextView)((LinearLayout)(v.getParent()).getParent()).getChildAt(0));
                            String txt =servicio.getText().toString();
                            if (!txt.endsWith(TXT_TO_DESMARCAR)) {
                                txt += TXT_TO_DESMARCAR;
                                servicio.setText(txt);
                            }
                        }
                    });*/
                    rbtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            TextView servicio = ((TextView)((LinearLayout)(buttonView.getParent()).getParent()).getChildAt(0));
                            String txt = null;
                            if (isChecked) {
                                txt =servicio.getText().toString();
                                if (!txt.endsWith(TXT_TO_DESMARCAR)) {
                                    txt += TXT_TO_DESMARCAR;
                                    servicio.setText(txt);
                                }
                            } else {
                                txt =servicio.getText().toString();
                                if (txt.endsWith(TXT_TO_DESMARCAR)) {
                                    txt = txt.substring(0, txt.length() - TXT_TO_DESMARCAR.length());
                                    servicio.setText(txt);
                                }
                            }
                        }
                    });
                    /*rbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (((RadioButton) v).isChecked()) {
                                ((RadioGroup)((RadioButton) v).getParent()).clearCheck();
                            }
                        }
                    });*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (num_empresas<5) {
                for (int i = num_empresas; i < 5 ; i++) {
                    RadioButton rbtn = ((RadioButton)((RadioGroup) groups[layout_pos].getChildAt(1)).getChildAt(i));
                    rbtn.setVisibility(View.GONE);
                }
            }
            ((RadioGroup) groups[layout_pos].getChildAt(1)).setVisibility(View.VISIBLE);
            Button btn = (Button) ((LinearLayout)groups[layout_pos].getParent()).getChildAt(6);
            if (btn.getVisibility() != View.VISIBLE) {
                btn.setVisibility(View.VISIBLE);
            }
            /*
            ((RadioGroup) groups[layout_pos].getChildAt(1)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((RadioButton) v).isChecked()) {
                        ((RadioGroup)((RadioButton) v).getParent()).clearCheck();
                    }
                }
            });*/
        }
    }

}
