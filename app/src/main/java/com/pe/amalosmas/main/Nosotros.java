package com.pe.amalosmas.main;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pe.amalosmas.R;
import com.pe.amalosmas.dao.RESTfulImpl;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * A simple {@link Fragment} subclass.
 */
public class Nosotros extends Fragment {


    public Nosotros() {
        // Required empty public constructor
    }

    private static final String COLLECCION = "nosotros";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_nosotros, container, false);
//        TextView am_f1_txt_va1l = view.findViewById(R.id.am_f1_txt_val1);
//        TextView am_f1_txt_va12 = view.findViewById(R.id.am_f1_txt_val1);
//        TextView am_f1_txt_va13 = view.findViewById(R.id.am_f1_txt_val1);
        new GetData().execute("3");
        new GetData().execute("0");
        new GetData().execute("1");
        new GetData().execute("2");
        /*int i = 0;
        TextView val = null;
        boolean continuar = true;
        for (String valor : lector.getObjetivos()
        ) {
            switch (i) {
                case 0:
                    val = am_f1_txt_va1l;
                    break;
                case 1:
                    val = am_f1_txt_va12;
                    break;
                case 2:
                    val = am_f1_txt_va13;
                    break;
                default:
                    continuar = false;
                    break;
            }
            if (!continuar) break;
            val.setText(valor);
            val.setVisibility(TextView.VISIBLE);
            i++;
        }*/
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles

        getActivity().setTitle(R.string.am_f1_label);
    }

    class GetData extends AsyncTask<String, Void, String[] > {
        ProgressDialog pd = new ProgressDialog(Nosotros.this.getContext());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setTitle("Cargando...");
            pd.show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            String stream = null;
            String tipo = params[0];
            String urlStr = RESTfulImpl.setURIBase(COLLECCION);
            urlStr += "&q={\"tipo\":"+tipo+"}&f={\"valor\":1}";
            //System.out.println(urlStr);
            RESTfulImpl lector = new RESTfulImpl();
            stream = lector.GetHTTPData(urlStr);
            return new String[] {stream, tipo};
        }

        @Override
        protected void onPostExecute(String[] s) {
            super.onPostExecute(s);
            //System.out.println(Arrays.toString(s));
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(s[0]);
                //System.out.println(jsonArray.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String result = "";
            switch (s[1]) {
                case "0":
                    TextView am_f1_txt_dsc_mision = getView().findViewById(R.id.am_f1_txt_dsc_dir);
                    try {
                        result = jsonArray.getJSONObject(0).getString("valor");
//                        JSONObject json = jsonArray.getJSONObject(0);
//                        result = json.getString("valor");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    am_f1_txt_dsc_mision.setText(result);
                    break;
                case "1":
                    TextView am_f1_txt_dsc_vision = getView().findViewById(R.id.am_f1_txt_dsc_vision);
                    try {
                        result = jsonArray.getJSONObject(0).getString("valor");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    am_f1_txt_dsc_vision.setText(result);
                    break;
                case "2":
                    //TextView am_f1_txt_dsc_vision = getView().findViewById(R.id.am_f1_txt_dsc_vision);
                    TextView[] valores = new TextView[3];
                    valores[0] = getView().findViewById(R.id.am_f1_txt_val1);
                    valores[1] = getView().findViewById(R.id.am_f1_txt_val2);
                    valores[2] = getView().findViewById(R.id.am_f1_txt_val3);

                    ConstraintLayout[] layouts = new ConstraintLayout[3];
                    layouts[0] = getView().findViewById(R.id.am_f1_layout_val1);
                    layouts[1] = getView().findViewById(R.id.am_f1_layout_val2);
                    layouts[2] = getView().findViewById(R.id.am_f1_layout_val3);
                    try {
                        for (int i =0; i<valores.length; i++) {
                            result = jsonArray.getJSONObject(i).getString("valor");
                            valores[i].setText(result);
                            layouts[i].setVisibility(ConstraintLayout.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case "3":
                    TextView am_f1_txt_dsc = getView().findViewById(R.id.am_f1_txt_dsc);
                    try {
                        result = jsonArray.getJSONObject(0).getString("valor");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    am_f1_txt_dsc.setText(result);
                    break;
            }
            pd.dismiss();
        }
    }
}
